export function getDiaryCache(DB: any, diaryData: any, theme: 'Dark' | 'Light'): Promise<string | null> {
  return DB.db.collection('cache').findOne({ diaryData, theme }, { photo: 1 }).then((cache: any) => {
    return new Promise((resolve, reject) => {
      if (cache) {
        resolve(cache.photo);
      } else {
        resolve(null);
      }
    });
  });
}

export function getMarksCache(DB: any, marksData: any, theme: 'Dark' | 'Light'): Promise<string | null> {
  return DB.db.collection('cache').findOne({ marksData, theme }, { photo: 1 }).then((cache: any) => {
    return new Promise((resolve, reject) => {
      if (cache) {
        resolve(cache.photo);
      } else {
        resolve(null);
      }
    });
  });
}
