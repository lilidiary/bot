﻿import { ObjectID } from 'mongodb';
import randomString from './data/random';

interface DB {
  db: any;
  collection: any;
}

class DB {
  constructor(db: any) {
    this.db = db;
    this.collection = db.collection('users');
  }

  byVK(vkID: number): Promise<any[]> {
    return this.collection.findOne({ vkID });
  }

  byID(_id: ObjectID): Promise<object[]> {
    return this.collection.findOne({ _id });
  }

  createUser(vkID: number, fname: string, sname: string): Promise<any> {
    const passcode = randomString(10);
    const toAdd = {
      passcode,
      vkID,
      fname,
      sname,
      logged: false,
      done: false,
      theme: 'Dark'
    };
    return this.db.collection('users').insertOne({ added: new Date(), ...toAdd });
  }

  getDiaryWithMarks(_id: ObjectID, startDate: Date, endDate: Date): Promise<any[]> {
    return this.db
      .collection('marks')
      .find({
        userId: _id,
        date: {
          $gte: startDate,
          $lte: endDate
        },
        mark: {
          $ne: null
        },
        old: { $ne: true }
      })
      .sort({
        date: 1
      })
      .toArray();
  }

  getDiaryFull(_id: ObjectID, startDate: Date, endDate: Date): Promise<any[]> {
    return this.db
      .collection('marks')
      .find({
        userId: _id,
        date: {
          $gte: startDate,
          $lte: endDate
        },
        old: { $ne: true }
      })
      .sort({
        date: 1
      })
      .toArray();
  }

  addLastMarksSent(_id: ObjectID, hash: number, startDate: Date, id: string): Promise<any> {
    return this.db.collection('users').updateOne(
      {
        _id
      },
      {
        $set: {
          marksHash: hash,
          marksDate: startDate,
          marksid: id
        }
      }
    );
  }

  addCount(_id: ObjectID): Promise<any> {
    return this.collection.updateOne(
      {
        _id
      },
      {
        $inc: {
          marksCount: 1
        }
      }
    );
  }

  addUser(user: object): Promise<any> {
    return this.db.collection('users').insertOne({ added: new Date(), ...user });
  }

  find(critation: object): Promise<object[]> {
    return this.db.collection('users').find(critation);
  }
}

export default DB;
