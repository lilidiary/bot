/*
  Calculate aspect ratio by height and width
*/
function calculateAspect(width: number, height: number) {
  return (height === 0) ? width : calculateAspect(height, width % height);
}

/*
  Return best sizes by aspect ratio
*/
function leadSize(height: number, width: number, aspect: number) {
  // 16:9 = hight:width
  // hight = 16 * width / 9
  // width = height * 9 / 16
  // height = (width * aspect)
  // width = ( height / aspect)
  const currentAscpect = height / width;
  const bestSize = {
    height,
    width
  };

  if (currentAscpect < aspect) {
    // height is smaller
    bestSize.height = width * aspect;
  } else {
    // width is smaller
    bestSize.width = height / aspect;
  }
  return bestSize;
}
export { calculateAspect, leadSize };
