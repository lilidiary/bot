export default (text: string): string => {
  let editedText = text;
  editedText = editedText.trim();
  editedText = editedText.replace(/\s\s+/g, ' ');
  editedText = editedText.replace(/,/g, ', ');
  editedText = editedText.replace(/:/g, ': ');
  editedText = editedText.replace(/§\s+/g, '§');
  editedText = editedText.replace(/\s+,\s,+/g, ',');
  editedText = editedText.replace(/\.,/g, ',');
  editedText = editedText.replace(/,$/g, '');
  editedText = editedText.replace(/егэ/gi, 'ЕГЭ');
  editedText = editedText.replace(/огэ/gi, 'ОГЭ');
  editedText = editedText.replace(/стр[^\s]/gi, 'стр ');
  editedText = editedText.replace(/\s+([.*+?$,:])/gm, '$1');
  editedText = editedText.replace(/([:,.])[^\s]/gm, '$1');
  editedText = editedText.trim();
  editedText = editedText.replace(/^([.,!?])+/g, '');
  editedText = editedText.replace(/[.,]+$/, '');

  editedText = editedText.charAt(0).toUpperCase() + editedText.slice(1);

  let finishedText = '';
  let quatWas = false;
  for (let char of editedText) {
    if (char === '"') {
      if (quatWas) {
        char = '»‎';
      } else {
        char = '«‎';
      }
      quatWas = !quatWas;
    }
    finishedText += char;
  }

  return finishedText;
};
