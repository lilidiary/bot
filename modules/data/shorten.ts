﻿import tr from '../../bot/translation.json';

const { short } = tr;
export default (str: string): string =>
  short[str.toLowerCase()] === undefined ? str : short[str.toLowerCase()];
