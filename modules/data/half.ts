﻿export function getHalfStarts(now: Date = new Date()): { firstHalf: Date, secondHalf: Date, endHalf: Date } {
  let firstHalf;
  let secondHalf;
  let endHalf;
  if (now < new Date(now.getFullYear(), 8, 0)) {
    firstHalf = new Date(now.getFullYear() - 1, 8, 2, -21);
    secondHalf = new Date(now.getFullYear(), 0, 0, -21);
    endHalf = new Date(now.getFullYear(), 5, 1);
  } else {
    firstHalf = new Date(now.getFullYear(), 8, 2, -21);
    secondHalf = new Date(now.getFullYear() + 1, 0, 1, -21);
    endHalf = new Date(now.getFullYear() + 1, 5, 1, -21);
  }
  return {
    firstHalf,
    secondHalf,
    endHalf
  };
}

export function getHalf(date: Date = new Date()): { title: string, startDate: Date, endDate: Date } {
  let startDate: Date;
  let endDate: Date;
  let title: string;
  const { firstHalf, secondHalf, endHalf } = getHalfStarts();
  if (date >= firstHalf && date < secondHalf) {
    startDate = firstHalf;
    endDate = secondHalf;
    title = '1 полугодие';
  } else {
    startDate = secondHalf;
    endDate = endHalf;
    title = '2 полугодие';
  }
  return {
    title,
    startDate,
    endDate
  };
}
