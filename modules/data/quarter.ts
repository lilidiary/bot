﻿export function getQuartersStarts(now: Date = new Date()): {
  firstQuarter: Date,
  secondQuarter: Date,
  thirdQuarter: Date,
  fourthQuarter: Date,
  endQuarter: Date
} {
  let firstQuarter: Date;
  let secondQuarter: Date;
  let thirdQuarter: Date;
  let fourthQuarter: Date;
  let endQuarter: Date;
  if (now < new Date(now.getFullYear(), 8, 0)) {
    firstQuarter = new Date(now.getFullYear() - 1, 8, 2, -21);
    secondQuarter = new Date(now.getFullYear() - 1, 9, 31, -21);
    thirdQuarter = new Date(now.getFullYear(), 0, 0, -21);
    fourthQuarter = new Date(now.getFullYear(), 2, 31, -21);
    endQuarter = new Date(now.getFullYear(), 5, 1);
  } else {
    firstQuarter = new Date(now.getFullYear(), 8, 2, -21);
    secondQuarter = new Date(now.getFullYear(), 12, 31, -21);
    thirdQuarter = new Date(now.getFullYear() + 1, 3, 1, -21);
    fourthQuarter = new Date(now.getFullYear() + 1, 3, 30, -21);
    endQuarter = new Date(now.getFullYear() + 1, 5, 1, -21);
  }
  return {
    firstQuarter,
    secondQuarter,
    thirdQuarter,
    fourthQuarter,
    endQuarter
  };
}

export function getQuarter(date: Date = new Date()): { title: string, startDate: Date, endDate: Date } {
  let startDate: Date;
  let endDate: Date;
  let title: string;
  const {
    firstQuarter,
    secondQuarter,
    thirdQuarter,
    fourthQuarter,
    endQuarter
  } = getQuartersStarts();
  if (date >= firstQuarter && date < secondQuarter) {
    startDate = firstQuarter;
    endDate = secondQuarter;
    title = 'Первая четверть';
  } else if (date >= secondQuarter && date < thirdQuarter) {
    startDate = secondQuarter;
    endDate = thirdQuarter;
    title = 'Вторая четверть';
  } else if (date >= thirdQuarter && date < fourthQuarter) {
    startDate = thirdQuarter;
    endDate = fourthQuarter;
    title = 'Третья четверть';
  } else {
    startDate = fourthQuarter;
    endDate = endQuarter;
    title = 'Четвертая четверть';
  }
  return {
    title,
    startDate,
    endDate
  };
}
