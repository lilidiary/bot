const lightScheme = {
  veryGood: 'green',
  good: '#5abc1e',
  bad: '#c67407',
  terrible: '#ca3e47'
};
const darkScheme = {
  veryGood: '#0c990c',
  good: '#5abc1e',
  bad: '#c67407',
  terrible: '#ca3e47'
};
export default (mark: number, theme: 'Dark' | 'Light' = 'Light'): string => {
  let color: string;
  let scheme = lightScheme;
  if (theme === 'Dark') {
    scheme = darkScheme;
  }
  if (mark >= 4) color = scheme.veryGood;
  else if (mark >= 3.5) color = scheme.good;
  else if (mark >= 2.7) color = scheme.bad;
  else color = scheme.terrible;
  return color;
};
