﻿import { MarksTemplate } from './marksTemplate';
import shorten from '../data/shorten';
import markColor from '../data/markColor';
import log from '../../log';
const svgToImg = require('svg-to-img');

const WIDTH = 261;
const RATIO = 16 / 9;
const MIN_HEIGHT = RATIO * WIDTH;
const arrAvg = (arr: number[]) => arr.reduce((a, b) => a + b, 0) / arr.length;

export function generateMarks(marks: any, title: string, theme: 'Dark' | 'Light' = 'Light'): Promise<String | Buffer> {
  const template = new MarksTemplate(WIDTH, MIN_HEIGHT, theme);
  let maxMarks = 0;
  for (const discipline of marks) {
    const disciplineAverage = arrAvg(discipline[1]);
    discipline.push(disciplineAverage);
    maxMarks = Math.max(maxMarks, discipline[1].length);
  }
  const newMarks = marks.sort((a: any, b: any) => {
    return a[2] - b[2];
  });

  for (let [discipline, disciplineMarks, avg] of newMarks) {
    const marksText = disciplineMarks.join(', ');
    const color = markColor(avg, theme);
    discipline = shorten(discipline).substring(0, 45);
    avg = avg.toFixed(2);
    template.addDiscipline(discipline, marksText, avg, color);
  }
  let width = WIDTH;
  if (maxMarks > 30) {
    width += (maxMarks - 30) * 8;
  }
  width = Math.max(width, WIDTH);
  template.width = width;

  const svg = template.getSVG({
    title,
    linkText: 'lilDiary.ru'
  });

  const generationStart = process.hrtime();

  return new Promise((resolve, reject) => {
    svgToImg.from(svg).toWebp({}).then((image: Buffer) => {
      const generationTime = process.hrtime(generationStart);
      log.debug(`Generation takes ${generationTime[1] / 1000000} ms`);
      resolve(image);
    }).catch((err: Error) => {
      reject(err);
    });
  });
}
