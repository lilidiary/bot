import { Template } from '../svgTemplates';
import { leadSize } from '../data/aspect';

export class MarksTemplate extends Template {
  private y: number;
  public width: number;
  private height: number;
  theme: 'Light' | 'Dark';

  constructor(width: number, height: number, theme: 'Light' | 'Dark' = 'Light') {
    super('marks');
    this.width = width;
    this.height = height;
    this.y = 30;
    this.theme = theme;
    this.loadFile(`marks${theme}.css`, 'styles');
  }

  addY(inc = 10) {
    this.y += inc;
  }

  addDiscipline(discipline: string, marks: string, avg: number, color: string) {
    this.insert(`<text x="10"
                     y="${this.y}"><tspan
                     sodipodi:role="line"
                     class="discipline"
                     style="fill: ${color}">${discipline}</tspan>
                     <tspan
                     sodipodi:role="line"
                     class="avg"
                     style="fill: ${color}">${avg}</tspan></text>
                <text><tspan
                     class="marks"
                     sodipodi:role="line"
                     x="11"
                     y="${this.y + 12}"
                     style="">${marks}</tspan></text>
                     <rect
                        class="line"
                        width="{*boxWidth*}"
                        height="0.1"
                        x="2"
                        y="${this.y + 14}" />`);
    this.addY(35);
  }

  getSVG(vars: any) {
    const height = Math.max(this.y + 30, this.height);
    const bestSizes = leadSize(height, this.width, 17 / 9);
    this.y = bestSizes.height;
    let logoY = bestSizes.height - 20;
    if (this.theme === 'Dark') {
      logoY = -100;
    }
    this.render({
      logoY,
      width: bestSizes.width,
      height: bestSizes.height,
      boxWidth: bestSizes.width - 4,
      linkY: bestSizes.height - 8,
      linkX: bestSizes.width - 10,
      pictureY: bestSizes.height - 67,
      pictureX: 0,
      pictureWidth: bestSizes.width,
      ...vars
    });
    return this.svg;
  }
}
