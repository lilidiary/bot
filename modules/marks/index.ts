import { Moment } from 'moment';
import tr from '../../bot/translation.json';
import { generateMarks } from './generateMarks';
import { getQuarter } from '../data/quarter';
import { proccessMarks } from './proccessMarks';
import { getMarksCache } from '../isChached';
import log from '../../log';

export function getMarksImage(user: any, vkID: number, DB: any, vk: any, startDate: Date | Moment, endDate: Date | Moment,
  theme: 'Dark' | 'Light', givenTitle: string | null = null): Promise<string | null> {
  return new Promise((resolve, reject) => {
    if (!(startDate instanceof Date)) {
      startDate = startDate.toDate();
    }
    if (!(endDate instanceof Date)) {
      endDate = endDate.toDate();
    }
    let title = givenTitle;
    if (!title) {
      title = getQuarter(startDate).title;
    }

    DB.getDiaryWithMarks(user._id, startDate, endDate)
      .then((marks: any[]) => {
        if (!marks) {
          resolve(null);
          return;
        }

        const { marksSorted } = proccessMarks(marks);
        getMarksCache(DB, marksSorted, theme).then((cachedPhoto) => {
          if (cachedPhoto) {
            resolve(cachedPhoto);
          } else {
            if (user.diaryData && user.diaryData.childs) {
              title = `${user.diaryData.childs[0][1]}, ${title}`;
            } else {
              title = `${user.fname} ${user.sname}`;
            }
            generateMarks(marksSorted, title, theme)
              .then((image) => {
                return vk.upload.messagePhoto({
                  source: image,
                  peer_id: vkID
                });
              })
              .then((photo) => {
                resolve(photo.toString());
                DB.db.collection('cache').insertOne({ theme, marksData: marksSorted, photo: photo.toString() });
              })
              .catch((err: Error) => {
                log.error(err);
                reject(err);
              });
          }
        })
          .catch((err: Error) => {
            log.error(err);
            reject(err);
          });
      });
  });
}
