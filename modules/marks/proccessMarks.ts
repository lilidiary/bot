export function proccessMarks(marks: any[]): { marksSorted: any[], hash: any } {

  const marksSorted = marks.reduce((marks, currentMark) => {
    const exists = marks.find(([discipline]: any) => {
      return discipline === currentMark.discipline;
    });
    if (exists) {
      exists[1].push(currentMark.mark);
    } else {
      marks.push([currentMark.discipline, [currentMark.mark]]);
    }
    return marks;
  }, []);

  return { marksSorted, hash: 0 };
}
