import moment from 'moment';
const MONTHS = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
];

export function weekInfo(weekDate: Date): { weekStart: Date, weekEnd: Date, title: string } {
  const weekStart = moment(weekDate).startOf('isoWeek');
  const weekEnd = moment(weekDate).endOf('isoWeek');

  let title: string;
  if (weekStart.month() === weekEnd.month()) {
    title = `С ${weekStart.date()} по ${weekEnd.date()} ${
      MONTHS[weekEnd.month()]
      }`;
  } else {
    title = `С ${weekStart.date()} ${
      MONTHS[weekStart.month()]
      } по ${weekEnd.date()} ${MONTHS[weekEnd.month()]}`;
  }

  return { title, weekStart: weekStart.toDate(), weekEnd: weekEnd.toDate() };
}
