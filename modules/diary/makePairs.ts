import moment from 'moment';
import fixSpell from '../data/fixSpell';
import shorten from '../data/shorten';

import log from '../../log';

moment.locale('ru');
interface Lesson {
  discipline: string;
  mark: number | null;
  disciplineText: string[];
  background: boolean;
  attendance: boolean;
  type: 'pair' | 'single';
}

interface OneDay {
  lessons: Lesson[];
  day: string;
  isToday: boolean;
}

function divideString(text: string, length: number): string[] {
  if (text.length <= length) {
    return [text];
  }
  let spacePoint = length;

  for (const charPointer of Array(10)
    .fill(length - 10)
    .reverse()
    .map((x, y) => x + y)) {
    if (text[charPointer] === ' ') {
      spacePoint = charPointer;
      break;
    }
  }

  const secondLine = text.substring(spacePoint);
  let result: string[] = [text.slice(0, spacePoint)];
  if (secondLine.length > length) {
    result = result.concat(divideString(secondLine, length));
  } else {
    result.push(secondLine);
  }
  return result;
}

function procHomework(homework: string) {
  const newHomework = fixSpell(homework);
  if (!newHomework) {
    return ['Нет домашнего задания'];
  }
  return divideString(newHomework, 65);
}

export function makePairs(rawDiary: any) {
  const marksStats = {
    bad: 0,
    all: 0,
    good: 0
  };


  const diaryToRender: OneDay[] = [];

  for (const dayNumber in rawDiary) {
    const lessons: any[] = rawDiary[dayNumber];
    const day: Date = new Date(dayNumber);

    const data: OneDay = {
      day: moment(day).format('dddd'),
      isToday: moment(day).isSame(moment(), 'date'),
      lessons: []
    };

    let chess = 1;
    let prevLesson: any = null;
    let remainLessons = Object.keys(lessons).length;

    for (const { discipline, mark, homework, room, comment, subject, remark, attendance } of lessons) {
      const isAttended = !(attendance && attendance[0] === 'Н');
      let disciplineText = homework;
      if (!disciplineText) {
        disciplineText = remark;
      }
      if (!disciplineText) {
        disciplineText = subject;
      }
      if (!disciplineText) {
        disciplineText = room;
      }
      if (!disciplineText) {
        disciplineText = comment;
      }
      if (!disciplineText) {
        disciplineText = '';
      }
      remainLessons -= 1;
      if (mark) {
        marksStats.all += 1;
        if (mark < 3) {
          marksStats.bad += 1;
        } else {
          marksStats.good += 1;
        }
      }
      // Check if it can be pair
      if (prevLesson
        && prevLesson.discipline === discipline
        && !(prevLesson.mark && mark)
        && !(prevLesson.homework && homework)) {

        data.lessons.push({
          discipline: shorten(discipline),
          type: 'pair',
          mark: mark ? mark : prevLesson.mark ? prevLesson.mark : '',
          attendance: isAttended,
          disciplineText: procHomework(prevLesson.disciplineText ? prevLesson.disciplineText : disciplineText ? disciplineText : ''),
          background: !!(chess % 2),
        });
        chess += 1;
        prevLesson = null;
        continue;
      }
      if (prevLesson) {
        chess += 1;
        data.lessons.push({
          discipline: shorten(prevLesson.discipline),
          mark: prevLesson.mark,
          attendance: prevLesson.isAttended,
          disciplineText: procHomework(prevLesson.disciplineText),
          background: !!((chess - 1) % 2),
          type: 'single'
        });

      }
      if (remainLessons === 0) {
        data.lessons.push({
          mark,
          discipline: shorten(discipline),
          attendance: isAttended,
          disciplineText: procHomework(disciplineText),
          background: !!(chess % 2),
          type: 'single'
        });
      }
      prevLesson = {
        discipline,
        mark,
        disciplineText,
        isAttended
      };
    }
    diaryToRender.push(data);
  }

  return { marksStats, diaryToRender, homeworkText: '' };
}
