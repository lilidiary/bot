﻿import { ObjectID } from 'mongodb';
import { Moment } from 'moment';
import { generateDiary } from './generateDiary';
import { VK, DbClass } from '../../types';
import { proccessLessons } from './proccessLessons';
import { weekInfo } from './weekInfo';
import { getDiaryCache } from '../isChached';
import log from '../../log';


export function getDiaryImage(userId: ObjectID, vkID: number, DB: DbClass, vk: VK, diaryDate: Date | Moment,
  theme: 'Dark' | 'Light', hideMarks = false): Promise<string | null> {
  return new Promise((resolve, reject) => {
    if (!(diaryDate instanceof Date)) {
      diaryDate = diaryDate.toDate();
    }
    const { weekStart, weekEnd, title } = weekInfo(diaryDate);

    DB.getDiaryFull(userId, weekStart, weekEnd)
      .then((diary: any[]) => {
        if (!diary.length) {
          resolve(null);
          return;
        }

        const proccessedLessons = proccessLessons(diary, hideMarks);
        getDiaryCache(DB, proccessedLessons, theme).then((cachedPhoto) => {
          if (cachedPhoto && process.env.NODE_ENV === 'production') {
            resolve(cachedPhoto);
            return;
          }
          generateDiary(proccessedLessons, title, theme)
            .then(({ image, homeworkText }) => {
              vk.upload.messagePhoto({
                source: image,
                peer_id: vkID
              })
                .then((photo: any) => {
                  resolve(photo.toString());
                  if (process.env.NODE_ENV === 'production') {
                    DB.db.collection('cache').insertOne({ theme, diaryData: proccessedLessons, photo: photo.toString() });
                  }
                })
                .catch((err: Error) => {
                  reject(err);
                  log.error(err);
                });
            }
            ).catch((err) => {
              reject(err);
              log.error(err);
            });
        }).catch((err: Error) => {
          log.error(err);
          reject(err);
        });
      }).catch((err: Error) => {
        reject(err);
        log.error(err);
      });
  });
}
