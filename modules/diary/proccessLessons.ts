export function proccessLessons(diaryData: any[], hidemarks = false): any {
  const diaryDataProccessed: any = {};
  for (const day of diaryData) {
    if (hidemarks) {
      day.mark = null;
    }
    if (diaryDataProccessed[day.date] !== undefined) {
      diaryDataProccessed[day.date].push(day);
    } else {
      diaryDataProccessed[day.date] = [day];
    }
  }
  return diaryDataProccessed;
}
