﻿import { DiaryTemplate } from './diaryTemplate';
import markColor from '../data/markColor';
import { makePairs } from './makePairs';
import shorten from '../data/shorten';
import log from '../../log';
const svgToImg = require('svg-to-img');

const WIDTH = 265;
const RATIO = 16 / 9;
const MIN_HEIGHT = RATIO * WIDTH;



export function generateDiary(diary: any, title: string | null = null, theme: 'Dark' | 'Light' = 'Light'): Promise<any> {

  const { diaryToRender, marksStats, homeworkText } = makePairs(diary);

  const template = new DiaryTemplate(WIDTH, MIN_HEIGHT, theme);
  for (const { day, lessons, isToday } of diaryToRender) {
    template.addDay(day, isToday);

    for (const { discipline, mark, type, disciplineText, background, attendance } of lessons) {
      let color: string = 'black';
      if (mark) {
        color = markColor(mark, theme);
      }
      template.addLesson(shorten(discipline).substring(0, type === 'pair' ? 12 : 19), mark, attendance,
        disciplineText.slice(0, 2), color, type, background);
    }
    template.addLine();

  }
  template.addFooter(`Оценок за неделю: ${marksStats.all}, из них плохих: ${marksStats.bad}, а хороших: ${marksStats.good}. До лета ${(
    (<any>new Date(2020, 5, 1) - <any>new Date()) /
    (1000 * 60 * 60 * 24)
  ).toFixed(0)} дней.`);
  const svg = template.getSVG({
    title
  });

  // const ratio = calculateAspect(y, WIDTH);
  // log.debug(`${WIDTH} x ${y}, ratio ${y / ratio}:${WIDTH / ratio} `);

  const generationStart = process.hrtime();
  return new Promise((resolve, reject) => {
    svgToImg.from(svg).toJpeg({}).then((image: Buffer) => {
      const generationTime = process.hrtime(generationStart);
      log.debug(`Generation takes ${generationTime[1] / 1000000} ms`);
      log.debug(`Size is ${image.toString().length / (8 * 1024)} MB`);
      resolve({ image, homeworkText });
    }).catch((err: Error) => {
      reject(err);
    });
  });
}
