import { Template } from '../svgTemplates';
import { leadSize } from '../data/aspect';

export class DiaryTemplate extends Template {
  private y: number;
  private width: number;
  private height: number;
  private theme: 'Dark' | 'Light';

  constructor(width: number, height: number, theme: 'Light' | 'Dark' = 'Light') {
    super('diary');
    this.width = width;
    this.height = height;
    this.y = 10;
    this.theme = theme;
    this.loadFile(`diary${theme}.css`, 'styles');
  }

  addY(inc = 10) {
    this.y += inc;
  }

  addDay(day: string, isToday = false) {
    this.addY(13);
    let today = '';
    if (isToday) {
      today = `
      <tspan
        class="today">Сегодня</tspan>`;
    }
    this.insert(`<text x="4"
                      y="${this.y}">
                   <tspan
                      class="dayName">${day}</tspan>
                      ${today}
                </text>`);
    this.addY();
  }

  addLesson(discipline: string, mark: number | null, attendance: boolean,
    homework: string[], color: string, type: 'pair' | 'single', backgroud = true) {
    if (backgroud) {
      this.insert(`<rect
                   class="chess"
                   width="{*rightBorder*}"
                   height="${8 * homework.length}"
                   x="2"
                   y="${this.y + 2 - 8}" />`);
    }
    const homeworkRendered = homework.reduce((homworkFull, hw, hwIndex) => {
      return `${homworkFull}
      <tspan
        x="70"
        dy="${hwIndex ? 6 : 0}"
        class="homework ${hw === 'Нет домашнего задания' ? 'empty' : ''}"
        >${hw}</tspan>`;
    }, '');
    this.insert(`<text x="7.2"
                          y="${this.y}">
                       <tspan
                          class="mark"
                          style="fill: ${mark ? color : (attendance ? '' : '#ca3e47')}">${mark ? mark : (attendance ? '' : 'Н')} </tspan>
                    </text>
                    <text x="10" y="${this.y}">
                       <tspan
                          class="discipline">${discipline}</tspan>
                        <tspan
                          class="attendance">${attendance ? '' : mark ? 'Н' : ''}</tspan>
                        <tspan
                          class="pair">${type === 'pair' ? '(Пара)' : ''}</tspan>
                    </text>
                    <text x="70" y="${this.y}" >
                      ${homeworkRendered}
                    </text>`);
    this.addY(homework.length * 8);

  }

  addLine() {
    this.insert(`<rect
                   class="line"
                   width="{*rightBorderLine*}}"
                   height="0.01"
                   x="2"
                   y="${this.y - 3}" />`);
    this.addY(2);
  }

  addFooter(text: string) {
    this.insert(`
      <text>
        <tspan
            sodipodi:role="line"
            x="2"
            y="${this.y + 10}"
            style="font-size:4"
            class="homework">${text}</tspan>
            </text>`);
    this.addY(10);
  }

  addWatermark(text: string) {
    this.insert(`<text>
            <tspan
                x="{*right*}"
                y="${this.y - 8}"
                class="link">${text}</tspan>
            </text>`);
  }

  getSVG(vars: any) {
    const height = Math.max(this.y + 25, this.height);
    const bestSizes = leadSize(height, this.width, 17 / 9);
    this.y = bestSizes.height;
    this.addWatermark('lilDiary.ru');
    let logoY = this.y - 20;
    if (this.theme === 'Dark') {
      logoY = -100;
    }
    this.render({
      logoY,
      width: bestSizes.width,
      height: this.y,
      rightBorder: bestSizes.width - 4,
      rightBorderLine: bestSizes.width - 6,
      right: bestSizes.width - 10,
      ...vars
    });
    return this.svg;
  }
}
