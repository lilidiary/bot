import fs from 'fs';
import path from 'path';


const dirname = path.join(__dirname, '/templates');
const fonts = fs.readFileSync(path.join(dirname, 'fonts.css'), 'utf-8');
const logo = fs.readFileSync(path.join(dirname, 'logo.base64.txt'), 'utf-8');

export class Template {
  public svg: string;
  public main: string;
  public images: any;
  constructor(name: string) {
    this.svg = fs.readFileSync(path.join(dirname, `${name}.svg`), 'utf-8');
    this.svg = this.svg.replace('{*fonts*}', fonts);
    this.svg = this.svg.replace('{*logo*}', logo);
    this.main = '';
    this.images = {};
  }

  insert(svgToAdd: string) {
    this.main += svgToAdd;
  }

  replaceVar(name: string, value: string) {
    this.svg = this.svg.split(`{*${name}*}`).join(value);
  }

  loadFile(name: string, replacement: string) {
    const image = fs.readFileSync(path.join(dirname, name), 'utf-8');
    this.images[replacement] = image;
  }

  renderImages() {
    for (const replacement in this.images) {
      this.replaceVar(replacement, this.images[replacement]);
    }
  }

  render(vars: any): string {
    this.svg = this.svg.replace('{*main*}', this.main);
    for (const replacement in vars) {
      this.replaceVar(replacement, vars[replacement]);
    }
    this.renderImages();
    return this.svg;
  }
}
