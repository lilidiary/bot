import { Markup } from 'nodejs-vk-bot';
import moment from 'moment';
import { getDiaryImage } from '../../modules/diary';
import { BotParams } from '../../types';
import log from '../../log';

const keyboard = Markup.keyboard(['Дневник']).inline();
export default ({ DB, vk, bot }: BotParams) => {
  return async (ctx) => {
    ctx.read();
    ctx.typing();
    if (ctx.button !== 'дневник') {
      ctx.reply(':)', null, keyboard);
      return;
    }
    log.debug('Requesting diary in chat');

    const chat = await DB.db
      .collection('chats')
      .findOne({ peerID: ctx.message.peer_id });
    let chatUsers: number[] = [];
    if (chat) {
      const now = moment();
      const lastSent = moment(chat.lastSent);
      if (chat.lastSent && now.diff(lastSent, 'minutes') < 10) {
        ctx.reply('Вы можете смотреть дневник не чаще 10 минут', null, keyboard);
        return;
      }

      if (!chat.users.includes(ctx.message.from_id)) {
        DB.db
          .collection('chats')
          .updateOne(
            { _id: chat._id },
            { $push: { users: ctx.message.from_id } }
          );
        chatUsers = [ctx.message.from_id, ...chat.users];
      } else {
        chatUsers = [...chat.users];
      }
    } else {
      ctx.reply(
        'Привет!',
        null,
        keyboard
      );
      DB.db.collection('chats').insertOne({
        peerID: ctx.message.peer_id,
        users: [ctx.message.from_id]
      });
      chatUsers = [ctx.message.from_id];
    }

    if (!chatUsers) {
      return;
    }

    const user = await DB.collection.findOne({
      vkID: ctx.message.from_id
    });


    if (!user || !user.logged) {
      ctx.reply(
        'Пусть эту кнопку нажмет человек, который уже подключен к нашему боту.',
        null,
        keyboard
      );
      return;
    }
    DB.db
      .collection('chats')
      .updateOne(
        { peerID: ctx.message.peer_id },
        { $set: { lastSent: new Date() } }
      );

    getDiaryImage(user._id, ctx.message.peer_id, DB, vk, new Date(), ctx.theme).then((photo) => {
      if (photo) {
        ctx.reply('', photo, keyboard);
      } else {
        ctx.reply('У нас нет данных об уроках на эту неделю. Возможно, сейчас каникулы или завуч еще не создал расписание', null, keyboard);
      }
    }).catch((err) => {
      ctx.reply('Произошла ошибка во время загрузки дневника. Попробуй еще раз', null, keyboard);
    });
  };
};
