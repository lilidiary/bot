import request from 'request';
import qs from 'querystring';
const yandexTtsUrl = 'https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize';
interface Options {
  ssml: string;
  token: string;
  speed?: number;
  folderId?: string;
  lang?: string;
  voice?: string;
  emotion?: 'neutral' | 'evil' | 'good';
}

const yandexTTS = function (options: Options): Promise<Buffer> {
  const params = {
    speed: 1,
    ...options
  };
  delete params.token;
  return new Promise((resolve, reject) => {
    request.post(yandexTtsUrl, {
      body: qs.stringify(params),
      encoding: null,
      headers: { Authorization: `Bearer ${options.token}` }
    }, (err, body, file) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(file);
    });
  });
};

export { yandexTTS };
