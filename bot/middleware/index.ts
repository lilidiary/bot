import proccessMessage from './proccessMessage';
import splitMessage from './splitMessage';
import debug from './debug';
import privacy from './privacy';
import { prohibitBot } from './prohibition';
import { BotParams } from '../../types';

export default (params: BotParams) => {
  splitMessage(params);
  debug(params);
  prohibitBot(params);
  privacy(params);
  proccessMessage(params);
};
