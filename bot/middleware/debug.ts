import { BotParams } from '../../types';

export default ({ bot, config }: BotParams) => {
  bot.use((ctx, next) => {
    if (process.env.NODE_ENV !== 'production' && !ctx.isAdmin) {
      return;
    }
    if (config.debug && ctx.isAdmin) {
      return;
    }
    next();
  });
};
