import chat from '../chat/index';
import { BotParams } from '../../types';
import log from '../../log';

export default ({ bot, vk, config, DB }: BotParams) => {
  const chatMiddleware = chat({ bot, DB, vk, config });

  bot.use((ctx: any, next: any) => {
    if (ctx.message.from_id !== ctx.message.peer_id) {
      log.debug('Message from chat');
      chatMiddleware(ctx);
      return;
    }


    next();
  });
};

