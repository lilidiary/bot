import { Markup } from 'nodejs-vk-bot';
import { BotParams } from '../../types';


export default ({ bot, DB }: BotParams) => {
  bot.use((ctx, next: any) => {
    if (ctx.message.out) {
      next();
      return;
    }
    if (!ctx.user.privacy) {
      if (ctx.message.text === 'Я согласен') {
        ctx.reply('Хорошо. ', null, Markup.keyboard(['Далее']));
        ctx.found = true;
        DB.db.collection('users').updateOne({ _id: ctx.user._id }, {
          $set: {
            privacy: true
          }
        }).then(() => {
          next();
        });
        return;
      }
      if (ctx.message.text === 'Я не согласен') {
        ctx.reply('Хорошо, вы всегда можете вернуться к нам нажав кнопку "Начать"', null, Markup.keyboard([Markup.button('Начать', 'positive')]));
        DB.db.collection('users').deleteOne({ _id: ctx.user._id });
        return;
      }

      ctx.reply(`
Мы соблюдаем №152-ФЗ «О персональных данных». lilDiary - это посредник между вашим электронным дневником и вами.  Поэтому вам необходимо ознакомиться с нашей политикой конфеденциальности https://lildiary.ru/privacy.pdf

Мы собираем, храним, обрабатываем ваши персональные данные.
Данные, который мы собираем: ФИО, ваш ip адрес, cookie (sessionid) с сайта вашего электронного дневника, оценки, расписание уроков, имена учителей, присутствие на уроке, домашнее задание, номер и букву класса, информацию о школе начиная с 2019.09.01
Вы всегда можете удалить свои данные нажатием кнопки "Выйти" или написав на почту privacy@lildiary.ru

Мы НЕ передаём данные третьим лицам
      `, null, Markup.keyboard(['Я согласен', 'Я не согласен'], { columns: 1 }));
    } else {
      next();
    }
  });
};

