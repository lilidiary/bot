import { BotParams } from '../../types';

const prohibitBot = ({ bot, config }: BotParams) => {
  bot.use((ctx, next) => {
    if (ctx.message.out) {
      next();
      return;
    }
    if (!config.work) {
      ctx.reply('Доступ временно закрыт.');
      return;
    }
    next();
  });
};
export { prohibitBot };

