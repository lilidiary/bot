﻿import tr from '../translation.json';
import { BotParams } from '../../types';

export default ({ bot, DB, vk, config }: BotParams) => {
  bot.use((ctx: any, next: any) => {
    if (ctx.message.out) {
      next();
      return;
    }
    ctx.read();
    next();
  });

  bot.use((ctx, next: any) => {
    if (ctx.user && !ctx.user.url) {
      ctx.scene.enter('url');
    } else {
      next();
    }
  });

  bot.use((ctx, next: any) => {
    if (ctx.user && ctx.user.logged && !ctx.user.done) {
      let fun = 0;
      if (ctx.user.fun) {
        fun = ctx.user.fun;
      }
      ctx.reply(tr.fun[fun]);
      if (fun !== tr.fun.length - 1) {
        fun += 1;
      }
      DB.collection.updateOne({ _id: ctx.user._id }, { $set: { fun } });
    }
    next();
  });
};
