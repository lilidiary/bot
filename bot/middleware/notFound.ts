﻿import { loginKeyboard } from '../keyboard';
import { BotParams } from '../../types';

export default ({ bot }: BotParams) => {
  bot.on((ctx) => {
    if (
      ctx.user &&
      ctx.user.url &&
      !ctx.user.logged) {
      ctx.reply(
        `Почти готово! Только осталось подключиться к lilDiary. Для этого перейдите по ссылке https://login.lildiary.ru/${ctx.user.passcode}
Предупреждаем, что это НЕофициальный портал госуслуг, он сделан специально для lilDiary`,
        null, loginKeyboard
      );
    }
  });
};
