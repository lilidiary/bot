﻿import { VkBot, Markup } from 'nodejs-vk-bot';

import scenes from './scenes/index';
import { contextExtender } from './contextExtender';
import middleware from './middleware/index';
import commands from './commands/index';
import distribution from './distribution/index';
import notFound from './middleware/notFound';

import admin from './admin/index';
import log from '../log';
import { BotParams } from '../types';

export default (params: any) => {
  const { config, DB, vk } = params;

  DB.db
    .collection('config')
    .findOne({ service: 'regions' })
    .then(({ regions }) => {
      const bot = new VkBot(config.vk.bot_token);

      const paramNext: BotParams = {
        vk,
        bot,
        regions,
        ...params
      };

      contextExtender(paramNext);
      scenes(paramNext);

      admin(paramNext);
      middleware(paramNext);
      commands(paramNext);
      distribution(paramNext);
      notFound(paramNext);

      bot.onPoll((ts: number) => {
        DB.db
          .collection('config')
          .updateOne({ service: 'bot' }, {
            $max: {
              'config.ts': Number(ts)
            }
          });
      });

      bot.startPolling(config.ts ? config.ts : null).then(() => {
        log.info('Started Long Polling');
        if (process.env.NODE_ENV === 'production') {
          for (const admin of config.admins) {
            bot.sendMessage(
              admin,
              `Бот запущен\nОкружение: ${process.env.NODE_ENV}\nВерсия Nodejs ${process.version}\nПлатформа ${process.platform}\nИзменения: ${config.commit}\nКоличество админов: ${config.admins.length}\nts: ${config.ts}`,
              null,
              Markup.keyboard(['Панель']).inline()
            );
          }
        }
      });
    })
    .catch((err: Error) => {
      log.error(err);
      log.info('Set working to false');
      process.exit();
    });
};
