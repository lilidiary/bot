import { BotParams } from '../../types';
import moment from 'moment';
import log from '../../log';
import { Markup } from 'nodejs-vk-bot';
moment.locale('ru');

const notifyNewMark = ({ bot, DB, config, vk }: BotParams) => {
  DB.collection.find({ done: true, settingsNotifications: { $ne: false } })
    .toArray()
    .then(async (users: any[]) => {
      for (const user of users) {
        if (process.env.NODE_ENV !== 'production' && !config.admins.includes(user.vkID)) {
          continue;
        }

        const marksStartDate = moment(user.doneTime).subtract(20, 'days').toDate();
        const marks = await DB.db
          .collection('marks')
          .find({
            userId: user._id,
            date: { $gte: marksStartDate },
            mark: { $ne: null },
            $or: [{ old: true, notifiedChange: { $ne: true } }, { old: false, notified: { $ne: true } }]
          })
          .toArray();

        if (!marks[0]) {
          continue;
        }
        for (const mark of marks) {
          let notificationText = '';
          const dateText = moment(mark.date).calendar().split(',')[0];
          if (mark.old && mark.notifiedChange === false) {
            if (mark.changedTo) {
              notificationText += `«${mark.discipline}» - ${dateText} учитель исправил ${mark.mark} на ${mark.changedTo} \n`;
            } else {
              notificationText += `«${mark.discipline}» - ${dateText} учитель удалил оценку ${mark.mark}\n`;
            }
            DB.db.collection('marks').updateOne({ _id: mark._id }, { $set: { notifiedChange: true } });
            continue;
          }

          if (user.diaryData && user.diaryData.type === 'parent') {
            notificationText += `«${mark.discipline}» - ${dateText} ребенок получил ${mark.mark}`;
          } else {
            notificationText += `«${mark.discipline}» - ${dateText} вы получили ${mark.mark}`;
          }
          if (mark.mark < 4) {
            if (mark.subject) {
              notificationText += `, тема урока ${mark.subject}`;
            }
            if (mark.homework) {
              notificationText += `, дз было: ${mark.homework}`;
            }
            if (mark.comment) {
              notificationText += `, замечание: ${mark.comment}`;
            }
          }
          if (mark.attendance && mark.attendance[0] === 'Н') {
            notificationText += ', но вас не было на уроке';
          }
          notificationText += '\n';
          DB.db.collection('marks').updateOne({ _id: mark._id }, { $set: { notified: true } });

          if (notificationText) {
            const theme = user.theme ? user.theme : 'Light';
            // getDiaryImage(user._id, user.vkID, DB, vk, mark.date, theme).then((photo) => {
            //   bot.sendMessage(user.vkID, notificationText, photo);
            // });
            bot.sendMessage(user.vkID, notificationText, '',
              Markup.keyboard([Markup.button('Посмотреть', 'primary', { button: mark.date })]).inline()
            );
          }
        }


      }
    }).catch((err: Error) => {
      log.error(err);
    });
};

export default (params: BotParams) => {
  return setInterval(() => {
    notifyNewMark(params);
  }, 1000 * 60); // 1 minute
};
