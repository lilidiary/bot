﻿import { ObjectID } from 'mongodb';
import tr from '../translation.json';
import { mainKeyboard } from '../keyboard';
import log from '../../log';

import { BotParams } from '../../types';

function notify({ bot, DB, config, vk }: BotParams) {
  DB.find({
    notify: { $ne: false },
    privacy: true
  }).toArray().then((users: any[]) => {
    for (const user of users) {
      log.debug('Notified user', user.fname);
      if (process.env.NODE_ENV !== 'production' && !config.admins.includes(user.vkID)) {
        continue;
      }
      user._id = new ObjectID(user._id);
      if (user.notification === 'logged') {
        const dataText: string =
          user.diaryData.type === 'pupil'
            ? `${user.diaryData.childs[0][1]}, ${user.diaryData.childs[0][2]}`
            : `Родитель, ${user.diaryData.fio}`;
        bot.sendMessage(user.vkID, tr.addedSession + dataText, null, mainKeyboard);
        for (const admin of config.admins) {
          bot.sendMessage(
            admin,
            `Авторизовался @id${user.vkID}(${user.fname} ${user.sname})`
          );
        }
      } else if (user.notification === 'done') {

        bot.sendMessage(user.vkID, tr.notifyDone, null, mainKeyboard);

      } else if (user.notification === 'logout') {
        bot.sendMessage(user.vkID, tr.logout + user.passcode, null, mainKeyboard);
      } else if (user.notification === 'taken') {
        bot.sendMessage(
          user.vkID,
          `Ух ты. Вашей ссылкой для входа воспользовался ${user.notificationDBTaken}`,
          null,
          mainKeyboard
        );
      } else if (user.notification === 'custom') {
        bot.sendMessage(user.vkID, user.notificationCustom, null, mainKeyboard);
      }
      DB.db.collection('users').updateOne(
        {
          _id: user._id
        },
        {
          $set: {
            notify: false,
            notification: null
          }
        }
      );
    }
  }).catch((err: Error) => {
    log.error(err);
  });
}
export default (params) => {
  return setInterval(() => {
    notify(params);
  }, 1000 * 2);
};
