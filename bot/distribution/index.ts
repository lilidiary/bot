﻿import notify from './notify';
import newMark from './newMark';
// import diary from './diary';
import { BotParams } from '../../types';

export default function (params: BotParams) {
  if (process.env.NODE_ENV !== 'production') {
    // return;
  }
  notify(params);
  newMark(params);
  // diary(bot, DB, log, config);
}
