import { Scene, Markup } from 'nodejs-vk-bot';
import { BotParams } from '../../types';
import { loginKeyboard } from '../keyboard';

export default ({ regions, DB }: BotParams) => {
  let regionsText = '';
  let count = 1;
  for (const region of regions) {
    regionsText += `${count}) ${region.name}\n`;
    count += 1;
  }
  const selectRegion = new Scene(
    'url',
    (ctx) => {
      ctx.scene.next();
      ctx.reply(`А теперь выбери свой регион (Число)\n ${regionsText}`, null, Markup.keyboard([
        Markup.button('Не могу войти', 'positive')
      ], { columns: 1 }));
    },
    (ctx) => {
      let region = ctx.message.text;
      region = Number.parseInt(region, 10);
      if (Number.isNaN(region) || region < 1 || region > regions.length) {
        ctx.reply('Ошибка. Повтори еще раз');
        ctx.scene.leave();
        ctx.scene.enter('url');
        return;
      }
      ctx.scene.leave();

      region = Number(region);
      if (region !== 13) {
        ctx.reply('Мы 100% поддерживаем Вологодскую область, но не уверены, что lilDiary работает в вашем регионе. Но вы можете попытаться зайти\nЕсли вы хотите одолжить свой аккаунт от дневника (только для тестирования нашей системы) - пишите https://vk.com/maxxx.pavlov');
      }
      region = regions[region - 1];
      ctx.reply(
        `Супер! Теперь осталось подключиться к lilDiary. Перейдите по ссылке и авторизуйтесь
          \nPS: Уже более 500 школьников Череповца пользуются lilDiary - не отставай 😉`,
        'doc-179799701_515322882',
        loginKeyboard);

      ctx.reply(`https://login.lildiary.ru/u_${ctx.user.passcode}`);

      DB.collection.updateOne(
        {
          _id: ctx.user._id
        },
        {
          $set: {
            url: region.url
          }
        }
      );
    }
  );
  return selectRegion;
};
