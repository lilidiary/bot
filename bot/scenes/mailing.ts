import { Scene, Markup, } from 'nodejs-vk-bot';
import log from '../../log';
import { BotParams } from '../../types';

const sceneMailing = ({ DB, bot }: BotParams) => {
  const exit = new Scene(
    'mailing',
    (ctx) => {
      ctx.scene.next();
      ctx.reply(
        'Хотите сделать рассылку?',
        null,
        Markup.keyboard([
          Markup.button('Да'),
          Markup.button('Нет')
        ]).oneTime()
      );
    },
    (ctx) => {
      if (ctx.message.text !== 'Да') {
        ctx.scene.leave();
        ctx.reply('Хорошо');
        return;
      }
      ctx.reply(`Введи сообщение рассылки. {*НАЗВАНИЕ_ПЕРЕМЕННОЙ*}
URL - ссылка авторизации
FNAME, SNAME - Имя, Фамилия
`, null, Markup.keyboard(['Отмена']));
      ctx.scene.next();
    }, (ctx) => {
      if (ctx.message.text === 'Отмена') {
        ctx.reply('Отмена');
        ctx.scene.leave();
        return;
      }
      const testUser = {
        passcode: 1234,
        fname: 'Максим',
        sname: 'Павлов'
      };
      const templateMailing = ctx.message.text;
      ctx.session.templateMailing = templateMailing;
      let exampleMailing = templateMailing.replace('{*URL*}', `login.lildiary.ru/u_${testUser.passcode}`);
      exampleMailing = exampleMailing.replace('{*FNAME*}', testUser.fname);
      exampleMailing = exampleMailing.replace('{*SNAME*}', testUser.sname);
      ctx.reply(exampleMailing, null, Markup.keyboard(['Далее', 'Отмена']));
      ctx.scene.next();
    }, (ctx) => {
      if (ctx.message.text !== 'Далее') {
        ctx.scene.leave();
        return;
      }
      ctx.reply('Выберите кому разослать', null, Markup.keyboard(['Всем', 'logged', 'Not logged', 'Отмена']));
      ctx.scene.next();
    }, (ctx) => {
      const mailingCriterion: any = {};
      switch (ctx.message.text) {
        case 'Всем':
          break;
        case 'logged':
          mailingCriterion.logged = true;
          break;
        case 'Not logged':
          mailingCriterion.logged = { $ne: true };
          break;
        default:
          ctx.scene.leave();
          return;
      }
      ctx.session.mailingCriterion = mailingCriterion;
      ctx.reply('Хотите разослать?', null, Markup.keyboard(['Да', 'Нет']));
      ctx.scene.next();
    }, (ctx) => {
      ctx.scene.leave();
      if (ctx.message.text !== 'Да') {
        ctx.reply('Ок');
        return;
      }
      DB.collection.find(ctx.session.mailingCriterion).toArray().then((users: any[]) => {
        for (const user of users) {
          let messageText = ctx.session.templateMailing.replace('{*URL*}', `login.lildiary.ru/u_${user.passcode}`);
          messageText = messageText.replace('{*FNAME*}', user.fname);
          messageText = messageText.replace('{*SNAME*}', user.sname);
          bot.sendMessage(user.vkID, messageText);
        }
        ctx.reply(`Рассылка закончиласть. ${users.length} сообщений`);
      }).catch((err: Error) => {
        ctx.reply('Ошибка при рассылке');
        log.error(err);
      });
    }
  );
  return exit;
};

export { sceneMailing };
