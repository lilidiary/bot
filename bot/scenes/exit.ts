import { Markup, Scene } from 'nodejs-vk-bot';
import { mainKeyboard, loginKeyboard } from '../keyboard';
import { BotParams } from '../../types';

export default ({ DB }: BotParams) => {
  const exit = new Scene(
    'exit',
    (ctx) => {
      ctx.scene.next();
      ctx.reply(
        'Ты можешь удалить информацию о себе и выйти из lilDiary',
        null,
        Markup.keyboard([
          Markup.button('Далее', 'negative'),
          Markup.button('Назад', 'positive')
        ]).oneTime()
      );
    },
    (ctx) => {
      if (ctx.message.text !== 'Далее') {
        ctx.scene.leave();
        let keyboard = mainKeyboard;
        if (
          ctx.user &&
          ctx.user.url &&
          !ctx.user.logged
        ) {
          keyboard = loginKeyboard;
        }
        ctx.reply(
          'Хорошо. Мы не будем удалять ваш аккаунт.',
          null,
          keyboard
        );
        return;
      }
      ctx.reply(
        'Если вы подтвердите это действие, то ваши данные, включая имя, оценки, информацию о школе и др. будут удалены. \nПолитика приватности https://lildiary.ru/privacy.pdf',
        null,
        Markup.keyboard(
          [
            Markup.button('Удалить мои данные', 'negative'),
            Markup.button('Я хочу пользоваться дневником', 'positive')
          ],
          { columns: 1 }
        ).oneTime()
      );
      ctx.scene.next();
    },
    (ctx) => {
      ctx.scene.leave();
      if (ctx.message.text !== 'Удалить мои данные') {
        let keyboard = mainKeyboard;
        if (
          ctx.user &&
          ctx.user.url &&
          !ctx.user.logged
        ) {
          keyboard = loginKeyboard;
        }
        ctx.reply(
          'Хорошо. Мы не будем удалять ваш аккаунт.',
          null,
          keyboard
        );
        return;
      }

      ctx.reply(
        'Мне очено жаль, что ты нас покинул. ',
        null,
        Markup.keyboard(['Начать']).oneTime()
      );
      DB.collection.deleteOne({
        vkID: ctx.message.from_id
      });
      DB.db.collection('marks').deleteMany({ userId: ctx.user._id });
    }
  );
  return exit;
};
