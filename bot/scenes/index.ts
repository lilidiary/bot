import { Stage, Session } from 'nodejs-vk-bot';
import Scenes from './scenes';
import { BotParams } from '../../types';

export default (params: BotParams) => {
  const session = new Session();
  params.bot.use(session.middleware());

  const scenes = Scenes(params);
  const stage = new Stage(...scenes);
  params.bot.use(stage.middleware());
};
