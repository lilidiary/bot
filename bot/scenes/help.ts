import { Markup, Scene } from 'nodejs-vk-bot';
import { BotParams } from '../../types';
import { loginKeyboard, mainKeyboard } from '../keyboard';

export default ({ DB, config, bot }: BotParams) => {
  const giveAccess = new Scene(
    'help',
    (ctx) => {
      ctx.reply('Напоминаем, что зайти можно через госуслуги (+14)\nЕсли пишет, что аккаунт не привязан, то попросите учителя чтобы привязал вам СНИЛС. Мы тут помочь не сможем');
      ctx.reply(
        'Хотите связаться с поддержкой?',
        null,
        Markup.keyboard(['Да', 'Нет'])
      );
      ctx.scene.next();
    },
    (ctx) => {
      if (ctx.message.text !== 'Да') {
        ctx.reply('Ок', null, loginKeyboard);
        ctx.scene.leave();
        return;
      }
      ctx.reply('Ожидайте, с вами свяжется администратор. Пока вы ждете, подробно опишите вашу проблему', null, Markup.keyboard(['Отмена']))
        .then(() => {
          ctx.reply('Если с вами долго никто не связывается - напишите @maxxx.pavlov (Мне)');
        });
      ctx.scene.next();

      for (const admin of config.admins) {
        bot.sendMessage(admin, `${ctx.userURL} запросил помощь. Свяжись с ним! https://vk.com/gim${config.vk.group_id}?sel=${ctx.message.peer_id}`);
      }
    }, (ctx) => {
      if (ctx.message.text === 'Отмена') {
        ctx.reply('Хорошо. ', null, loginKeyboard);
        ctx.scene.leave();
      }
      if (ctx.user.logged) {
        ctx.scene.leave();
        ctx.reply('Можете пользоваться дневником. Нажмите "Оценки"', null, mainKeyboard);
      }
    }
  );
  return giveAccess;
};
