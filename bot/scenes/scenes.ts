﻿import exit from './exit';
import region from './region';
import help from './help';
import { sceneMailing } from './mailing';
import { BotParams } from '../../types';

export default (params: BotParams) => {
  return [exit(params), region(params), help(params), sceneMailing(params)];
};
