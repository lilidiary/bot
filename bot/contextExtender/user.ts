import tr from '../translation.json';
import { BotParams } from '../../types';
import log from '../../log';


const userExtender = ({ bot, DB, vk, config }: BotParams) => {

  bot.use((ctx, next: any) => {
    if (ctx.message.out) {
      next();
      return;
    }
    DB.byVK(ctx.message.from_id).then((user: any) => {
      ctx.user = user;
      ctx.done = user ? user.done : false;
      next();
    }).catch((err: Error) => {
      log.error(err);
      ctx.reply('Ошибка. Повторите еще раз');
    });
  });


  bot.use((ctx, next: any) => {
    if (ctx.message.out) {
      next();
      return;
    }

    if (!ctx.user) {
      vk.api.users
        .get({
          user_ids: ctx.message.from_id
        })
        .then(([userInfo]) => {
          DB.createUser(ctx.message.from_id, userInfo.first_name, userInfo.last_name).then((doc: any) => {
            const [user] = doc.ops;
            if (!user) {
              ctx.reply(
                `${tr.helloMessage}
                Ошибка, попробуйте еще раз написать Начать :) `
              );
              return;
            }
            ctx.user = {
              _id: user._id, fname: userInfo.first_name,
              sname: userInfo.last_name,
              logged: false,
              done: false,
              vkID: ctx.message.from_id
            };
            next();
          });
          ctx.reply(tr.helloMessage);
        })
        .catch((err: Error) => {
          ctx.reply(
            `${tr.helloMessage}
            Ошибка, попробуйте еще раз написать Начать :) `
          );
          log.error(err);
        });
    } else {
      next();
    }
  });
};

export { userExtender };
