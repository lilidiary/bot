import { APIextender } from './api';
import { userExtender } from './user';
import foundMessage from './foundMessage';
import { dataExtender } from './dataExtender';
const contextExtender = (params) => {
  foundMessage(params);
  APIextender(params);
  userExtender(params);
  dataExtender(params);
};

export { contextExtender };

