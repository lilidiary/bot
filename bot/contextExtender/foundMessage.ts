import { BotParams } from '../../types';
import log from '../../log';

export default ({ bot, vk, config, DB }: BotParams) => {
  const found = {};

  bot.use((ctx: any, next: any) => {
    if (ctx.message.out) {
      found[ctx.message.to_id] = true;
      return;
    }
    if (ctx.message.type === 'message_typing_state') {
      if (ctx.message.from_id < 0) {
        if (ctx.message.state === 'typing') {
          found[ctx.message.to_id] = true;
        } else {
          found[ctx.message.to_id] = true;
        }
      }
      return;
    }
    ctx.found = !!found[ctx.message.peer_id];
    next();
  });
};

