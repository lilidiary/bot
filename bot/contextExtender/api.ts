import { BotParams } from '../../types';
// @ts-ignore
const APIextender = ({ bot, vk, config }: BotParams) => {
  bot.use((ctx: any, next: any) => {
    ctx.typing = (): Promise<any> => {
      return vk.api.messages.setActivity({
        group_id: config.vk.group_id,
        peer_id: ctx.message.peer_id,
        type: 'typing'
      });
    };

    ctx.read = (): Promise<any> => {
      return vk.api.messages.markAsRead({
        group_id: config.vk.group_id,
        peer_id: ctx.message.peer_id
      });
    };
    ctx.sendAdmin = (text: string) => {
      for (const admin of config.admins) {
        bot.sendMessage(admin, text);
      }
    };
    ctx.button = '';
    if (ctx.message.payload) {
      const { button } = JSON.parse(ctx.message.payload);
      if (button) {
        ctx.button = button.toLowerCase();
      }
    }
    next();
  });
};

export { APIextender };
