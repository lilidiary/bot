import { BotParams } from '../../types';
const dataExtender = ({ bot, vk, config }: BotParams) => {
  bot.use((ctx: any, next: any) => {
    ctx.theme = ctx.user.theme ? ctx.user.theme : 'Light';
    ctx.userURL = `@id${ctx.message.from_id} (${ctx.user.fname} ${ctx.user.sname})`;
    next();
  });
};

export { dataExtender };
