type levels = 'logged' | 'done' | 'notLogged' | 'all';

export function permit(level: levels) {
  return (ctx, next) => {
    switch (level) {
      case 'logged':
        if (ctx.user.logged) {
          next();
        }
        break;
      case 'done':
        if (ctx.user.done) {
          next();
        }
        break;
      case 'notLogged':
        if (!ctx.user.logged) {
          next();
        }
        break;
      case 'all':
        next();
        break;
      default:
        break;
    }
  };
}
