import { Markup } from 'nodejs-vk-bot';

// import mailing from './commands/mailing';
import purge from './commands/purgeCache';
import stats from './commands/stats';
import log from '../../log';
import subs from './commands/subs';

import { BotParams } from '../../types';

export default (ctx, next: any, pars: BotParams) => {
  if (!ctx.isAdmin) {
    next();
    return;
  }

  const params = { ctx, ...pars };

  const { vk, config, bot, DB } = params;
  if (subs(params)) {
    return;
  }
  if (purge({ DB, ctx })) {
    return;
  }
  if (stats({ DB, ctx, config })) {
    return;
  }

  if (ctx.button === 'остановить') {
    process.exit(0);
  }
  if (ctx.button === 'рассылка') {
    ctx.scene.enter('mailing');
    return;
  }
  if (ctx.message.text.toLowerCase() === 'панель') {
    ctx.reply(
      'lilDiary Dashboard',
      null,
      Markup.keyboard(
        [
          'Отладка',
          'Статистика',
          'work',
          'Сбросить кэш',
          'Тема рассылка',
          'Подписка',
          Markup.button('Рассылка', 'negative'),
          Markup.button('Остановить', 'negative'),
          'Назад'
        ],
        { columns: 2 }
      )
    );
    return;
  }

  if (ctx.button === 'отладка' && process.env.NODE_ENV === 'production') {
    if (!config.debug) {
      ctx.reply('Режим отладки включен');
      config.debug = true;
    } else {
      ctx.reply('Режим отладки выключен');
      config.debug = false;
    }
    return;
  }
  if (ctx.button === 'work' && process.env.NODE_ENV === 'production') {
    if (!config.work) {
      ctx.reply('Режим work включен');
      config.work = true;
    } else {
      ctx.reply('Режим work выключен');
      config.work = false;
    }
    return;
  }

  next();
};
