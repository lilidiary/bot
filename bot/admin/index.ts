import adminMiddleware from './middleware';
import commands from './commands';
import { BotParams } from '../../types';

export default (params: BotParams) => {
  params.bot.use(adminMiddleware(params));
  params.bot.use((ctx, next: any) => commands(ctx, next, params));
};
