import { BotParams } from '../../types';

export default ({ config }: BotParams) => {
  return (ctx, next: any) => {
    if (config.admins.includes(ctx.message.peer_id)) {
      ctx.isAdmin = true;
    }

    next();
  };
};
