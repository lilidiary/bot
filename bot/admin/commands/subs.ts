
const text = `
lilDiary - некомерческий проект, созданный группой школьников для школьников и их родителей
В знак благодарности за проделанную работу подпишитесь на нашу группу
Спасибо😌
`;
export default ({ DB, ctx, vk, config, bot }: any) => {
  if (ctx.button === 'подписка') {
    DB.collection.find({ logged: true, isMember: 0 }).toArray().then((users: any[]) => {
      for (const user of users) {
        bot.sendMessage(user.vkID, text, 'photo-179799701_457239377');
      }
    });

    return true;
  }
};
