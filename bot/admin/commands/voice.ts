import { yandexTTS } from '../../voice';

export default ({ DB, ctx, vk, config, bot }: any) => {
  if (ctx.button === 'голос') {
    DB.collection.find({}).toArray().then((users: any[]) => {
      yandexTTS({
        ssml: `
      <speak>
        <s>Привет! Я бот lilDiary. Рад поговор+ить!</s>
        <p><s>К сожалению, сейчас я работаю не для всех.</s> Если ты не успел войти, или у просто не можешь пользоваться дневником <break time="15ms"/> не переживай - мои создатели все починят</p> <break time="1s"/>
        <p><s>Кстати, теперь к меня появился голос <break time="1s"/></s>
        <s>Чекни мой последний пост</s><break time="2s"/></p>
      </speak>`,
        folderId: config.folderId,
        speed: 1,
        token: config.speechToken
      }).then(async (file) => {
        for (const user of users) {
          const id = await vk.upload.audioMessage({
            source: file,
            peer_id: user.vkID
          }).catch((err) => {
            ctx.reply('Ошибка');
          });
          if (id) {
            bot.sendMessage(user.vkID, '', id.toString());
          }
        }
      });
    });
    return true;
  }
};
