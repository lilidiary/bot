
export default ({ DB, ctx, vk, config, bot }: any) => {
  if (ctx.button === 'рейтинг') {
    DB.collection.find({ logged: true }).toArray().then((users: any[]) => {
      for (const user of users) {
        let text = 'Ух ты, уже прошло 2 месяца учёбы. Жесть! Надеюсь, ты хорошо учился в это время 😙\n';
        text += 'Мы решили подвести итог первой четверти и создали анонимый рейтинг учеников!\n';
        if (user.classYear >= 10) {
          if (user.ratingPlaceClass < 20) {
            text += `В этой четверти ты занимаешь ${user.ratingPlaceClass + 1} место среди 10-11 классов. Молодец!`;
          } else {
            text += `В этой четверти ты занимаешь ${user.ratingPlaceClass + 1} место среди 10-11 классов`;
          }
        } else {
          if (user.ratingPlaceClass < 20) {
            text += `В этой четверти ты занимаешь ${user.ratingPlaceClass + 1} место. Молодец!`;
          } else {
            text += `В этой четверти ты занимаешь ${user.ratingPlaceClass + 1} место `;
          }
        }
        bot.sendMessage(user.vkID, text).then(() => {
          bot.sendMessage(user.vkID, 'Ты можешь выложить свой результат в истории. Для этого перейди в наше приложение: vk.com/app7183418_498148588');
        });
      }
    });
    return true;
  }
};
