export default ({ DB, ctx, config }) => {
  if (ctx.button === 'статистика') {
    let response: string = '';
    DB.collection
      .find({})
      .count()
      .then((count: number) => {
        response += `Count of users ${count}\n`;
        return DB.collection.find({ logged: true }).count();
      })
      .then((count: number) => {
        response += `Count of logged users ${count}\n`;
        return DB.collection.find({ done: true }).count();
      })
      .then((count: number) => {
        response += `Count of done users ${count}\n`;
        return DB.db.collection('logs').find({ service: 'auth-server', type: 'submit' }).count();
      }).then((count: number) => {
        response += `Tries to login: ${count} (auth-server)\n`;
        return DB.db.collection('logs').find({ service: 'auth-server', type: 'connection' }).count();
      }).then((count: number) => {
        response += `login vists: ${count} (auth-server)\n`;
        return DB.db.collection('marks').aggregate([{
          $group: {
            _id: '$mark',
            mark: { $first: '$mark' },
            count: { $sum: 1 }
          }
        }, {
          $sort: {
            mark: 1
          }
        }]).toArray();
      }).then((count: any[]) => {
        for (const markCount of count) {
          response += `${markCount.mark} - ${markCount.count}\n`;
        }
        return DB.db.collection('users').aggregate([{
          $group: {
            _id: { $arrayElemAt: [{ $arrayElemAt: ['$diaryData.childs', 0] }, 2] },
            count: { $sum: 1 }
          }
        }]).toArray();
      }).then((count: { _id: string, count: number }[]) => {
        for (const schoolCount of count) {
          response += `${schoolCount._id} - ${schoolCount.count}\n`;
        }
        response += `ts: ${config.ts}`;
      })
      .then(() => {
        ctx.reply(response);
      })
      .catch((err: Error) => {
        ctx.reply('Ошибка во время подсчёта');
      });
    return true;
  }
};
