export default ({ DB, ctx }) => {
  if (ctx.button === 'сбросить кэш') {
    DB.collection.updateMany({}, { $set: { marksHash: null } });
    DB.db.collection('cache').deleteMany({});
    DB.collection.updateMany({}, {
      $set: {
        marksHash: null
      }
    });
    ctx.reply('Кэш сброшен');
    return true;
  }
};
