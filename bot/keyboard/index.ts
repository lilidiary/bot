import { Markup } from 'nodejs-vk-bot';

const mainKeyboard = Markup.keyboard(
  [
    Markup.button('Дневник', 'primary'),
    Markup.button('Оценки', 'primary'),
    Markup.button('Другое'),
    Markup.button('Следующая неделя')
  ],
  {
    columns: 2
  }
);

const loginKeyboard = Markup.keyboard(
  [
    Markup.button('Не могу войти'),
    Markup.button('Выйти', 'negative'),
  ],
  {
    columns: 1
  }
);
const settingsKeyboard = Markup.keyboard(
  [
    Markup.button('Тема'),
    Markup.button('Четверти / Полугодия'),
    Markup.button('Уведомления'),
    Markup.button('Поддержка'),
    Markup.button('Выйти', 'negative'),
    Markup.button('Назад', 'positive')
  ],
  {
    columns: 1
  }
);
const timeframeKeyboard = Markup.keyboard([
  Markup.button('Четверти'),
  Markup.button('Полугодия'),
  Markup.button('Назад', 'positive')
], { columns: 1 });
const otherKeyboard = Markup.keyboard(
  [
    '1 полугодие',
    '2 полугодие',
    '1 четверть',
    '2 четверть',
    '3 четверть',
    '4 четверть',
    Markup.button('Настройки', 'primary'),
    Markup.button('Назад', 'positive')
  ],
  { columns: 2 }
);

export { mainKeyboard, loginKeyboard, otherKeyboard, settingsKeyboard, timeframeKeyboard };
