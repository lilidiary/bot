import { mainKeyboard, otherKeyboard } from '../keyboard';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import log from '../../log';

export default ({ bot }: BotParams) => {
  bot.command('Другое', permit('done'), (ctx) => {
    ctx.reply(
      'Другое',
      null,
      otherKeyboard
    );
  });

  bot.command(['Назад', 'Нет'], (ctx) => {
    if (ctx.user.logged) {
      ctx.reply('Ок', null, mainKeyboard);
    }
  });
};
