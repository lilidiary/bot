﻿import tr from '../translation.json';
import { getMarksImage } from '../../modules/marks';
import { getQuarter } from '../../modules/data/quarter';
import { getHalf } from '../../modules/data/half';
import { permit } from '../permissions';

import { BotParams } from '../../types';
import log from '../../log';

export default ({ bot, DB, vk }: BotParams) => {

  bot.command(['Итоговые', 'Оценки'], permit('done'), (ctx) => {
    ctx.typing();
    log.debug(`Summary marks ${ctx.message.peer_id}`);
    let { startDate, endDate, title } = getQuarter();

    if (ctx.user.timeFrame === 'half') {
      ({ startDate, endDate, title } = getHalf());
    }
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, startDate, endDate, ctx.theme, title).then((photo) => {
      if (photo) {
        ctx.reply('', photo).then(() => {
          if (!ctx.user.marksCount) {
            ctx.reply(tr.firstMarks);
          }
          if (ctx.use.marksCount === 5) {
            ctx.reply('Нравится дневниик lilDiary? Так почему бы не скинуть ссылочку на него своим друзьям');
          }
          if (ctx.user.marksCount === 10) {
            ctx.reply(
              'Супер, да? А знаешь, что ты еще можешь - ты можешь добавить lilDiary в беседу класса, чтобы все могли посмотреть дз\nДобавь: https://vk.com/app6441755_-179799701'
            );
          }
        });
      } else {
        ctx.reply('У вас нет оценок за этот промежуток времени');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок :(');
    });
    DB.addCount(ctx.user._id);
  });
};
