import settings from './settings';
import marks from './marks';
import diary from './diary';
import look from './look';
import quarter from './quarter';
import half from './half';
import other from './other';
import { theme } from './theme';
import test from './test';
import { BotParams } from '../../types';

export default (params: BotParams) => {
  settings(params);
  marks(params);
  diary(params);
  look(params);
  quarter(params);
  half(params);
  other(params);
  theme(params);
  if (process.env.NODE_ENV === 'development') {
    test(params);
  }
};
