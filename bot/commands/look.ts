import { getDiaryImage } from '../../modules/diary';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import moment from 'moment';
import log from '../../log';

export default ({ bot, DB, vk }: BotParams) => {
  bot.command('Посмотреть', permit('done'), (ctx) => {
    log.debug('look request');
    if (!ctx.button) {
      ctx.reply('Нажмите на кнопку под сообщением');
      return;
    }
    ctx.typing();
    const diaryDate = new Date(ctx.button);

    getDiaryImage(ctx.user._id, ctx.user.vkID, DB, vk, diaryDate, ctx.theme).then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У нас нет данных об уроках');
      }
    }).catch((err) => {
      ctx.reply('Произошла ошибка во время загрузки дневника. Попробуй еще раз :)');
    });
  });
};
