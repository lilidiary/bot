import { BotParams } from '../../types';
import { Markup } from 'nodejs-vk-bot';

export default (params: BotParams) => {
  const { bot, DB, vk, config } = params;
  bot.command(['test'], (ctx) => {
    ctx.reply('Смотри какая клава', null, Markup.keyboard(['test']).inline());
    ctx.reply(JSON.stringify(ctx.client_info));
  });
};
