import { Markup } from 'nodejs-vk-bot';
import { mainKeyboard } from '../keyboard';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import log from '../../log';

export function theme({ bot, DB }: BotParams) {
  bot.command('Тема', permit('done'), (ctx) => {
    ctx.reply(
      'Другое',
      null,
      Markup.keyboard(
        [
          'Светлая тема',
          'Тёмная тема',
          Markup.button('Назад', 'positive')
        ],
        { columns: 1 }
      )
    );
  });

  bot.command('Светлая тема', (ctx) => {
    if (ctx.user.theme === 'Light') {
      ctx.reply('У вас и так светлая тема :)', null, mainKeyboard);
    } else {
      ctx.reply('Теперь у вас светлая тема', null, mainKeyboard);
      DB.collection.updateOne({ _id: ctx.user._id }, {
        $set: {
          theme: 'Light',
          marksHash: null
        }
      });
    }
  });
  bot.command(['Тёмная тема', 'Темная тема'], (ctx) => {
    if (ctx.user.theme === 'Dark') {
      ctx.reply('У вас и так тёмная тема :)', null, mainKeyboard);
    } else {
      ctx.reply('Теперь у вас тёмная тема', null, mainKeyboard);
      DB.collection.updateOne({ _id: ctx.user._id }, {
        $set: {
          theme: 'Dark',
          marksHash: null
        }
      });
    }
  });
}
