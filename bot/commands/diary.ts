﻿import { getDiaryImage } from '../../modules/diary';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import moment from 'moment';
import log from '../../log';

export default ({ bot, DB, vk }: BotParams) => {
  bot.command('Дневник', permit('done'), (ctx) => {
    log.debug('Diary request');
    ctx.typing();
    getDiaryImage(ctx.user._id, ctx.user.vkID, DB, vk, new Date(), ctx.theme).then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У нас нет данных об уроках на эту неделю. Возможно, сейчас каникулы или завуч еще не создал расписание');
      }
    }).catch((err) => {
      ctx.reply('Произошла ошибка во время загрузки дневника. Попробуй еще раз :)');
    });
  });

  bot.command('Следующая неделя', permit('done'), (ctx) => {
    log.debug('Diary request');
    ctx.typing();

    getDiaryImage(ctx.user._id, ctx.user.vkID, DB, vk, moment().add(7, 'days'), ctx.theme).then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У нас нет данных об уроках на следующую неделю. ');
      }
    }).catch((err) => {
      ctx.reply('Произошла ошибка во время загрузки дневника. Попробуй еще раз :)');
    });
  });
};
