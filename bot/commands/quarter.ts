﻿import { getQuartersStarts } from '../../modules/data/quarter';
import { getMarksImage } from '../../modules/marks';
import { BotParams } from '../../types';
import { permit } from '../permissions';
import log from '../../log';

export default (params: BotParams) => {
  const { bot, DB, vk } = params;
  const {
    firstQuarter,
    secondQuarter,
    thirdQuarter,
    fourthQuarter,
    endQuarter
  } = getQuartersStarts();

  bot.command('1 четверть', permit('done'), (ctx) => {
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, firstQuarter, secondQuarter, ctx.theme, '1 Четверть').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за первую четверть');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за первую четверть');
    });
  });

  bot.command('2 четверть', permit('done'), (ctx) => {
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, secondQuarter, thirdQuarter, ctx.theme, '2 Четверть').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за вторую четверть');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за вторую четверть');
    });
  });
  bot.command('3 четверть', permit('done'), (ctx) => {
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, thirdQuarter, fourthQuarter, ctx.theme, '3 Четверть').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за третью четверть');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за третью четверть');
    });
  });
  bot.command('4 четверть', permit('done'), (ctx) => {
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, fourthQuarter, endQuarter, ctx.theme, '4 Четверть').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за четвертую четверть');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за последнюю');
    });
  });
};
