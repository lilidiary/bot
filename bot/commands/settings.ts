﻿import { settingsKeyboard, timeframeKeyboard, mainKeyboard } from '../keyboard';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import log from '../../log';
import { Markup } from 'nodejs-vk-bot';

export default ({ bot, DB }: BotParams) => {
  bot.command('Настройки', permit('logged'), (ctx) => {
    ctx.reply(
      'Настройки',
      null,
      settingsKeyboard
    );
  });

  bot.command('Уведомления', (ctx) => {
    if (!ctx.client_info.inline_keyboard) {
      ctx.reply('Вам нужна последняя версия Вконтакте на телефоне');
    }
    if (ctx.user.settingsNotifications || ctx.user.settingsNotifications === null || ctx.user.settingsNotifications === undefined) {
      ctx.reply('Вы можете отключить уведомления',
        null, Markup.keyboard([Markup.button('Отключить', 'negative', { button: 'settings-notification-off' })]).inline());
    } else {
      ctx.reply('Вы можете включить уведомления',
        null, Markup.keyboard([Markup.button('Включить', 'positive', { button: 'settings-notification-on' })]).inline());
    }

  });
  bot.command(['Отключить', 'Включить'], (ctx) => {
    if (ctx.button === 'settings-notification-off') {
      ctx.reply('Уведомления отключены', null,
        Markup.keyboard([Markup.button('Включить', 'positive', { button: 'settings-notification-on' })]).inline());
      DB.collection.updateOne({ _id: ctx.user._id }, { $set: { settingsNotifications: false } });
    }
    if (ctx.button === 'settings-notification-on') {
      ctx.reply('Уведомления включены');
      DB.collection.updateOne({ _id: ctx.user._id }, { $set: { settingsNotifications: true } });
    }
  });
  bot.command('Добавить родителя', (ctx) => {
    if (!ctx.user.done) return;
    ctx.reply('Функция  удалена');
  });

  bot.command('Четверти / Полугодия', (ctx) => {
    ctx.reply(`Вы можете сменить промежуток выставления оценок в вашей школе, сейчас у вас ${ctx.user.timeframe === 'half' ? 'полугодия' : 'четверти'}`, null, timeframeKeyboard);
  });

  bot.command('Поддержка', (ctx) => {
    ctx.reply('Возникли какие-то вопросы при использовании бота lilDiary? Тогда почитай мою страницу или напиши @maxxx.pavlov (Создателю)',
      null, mainKeyboard);
  });

  bot.command('Полугодия', (ctx) => {
    if (ctx.user.timeframe === 'half') {
      ctx.reply('У вас и так по полугодиям', null, mainKeyboard);
    } else {
      ctx.reply('Всё, теперь у вас по полугодиям', null, mainKeyboard);
      DB.collection.updateOne({ _id: ctx.user._id }, {
        $set: {
          timeframe: 'half'
        }
      });
    }
  });
  bot.command('Четверти', (ctx) => {
    if (ctx.user.timeframe === 'quarter') {
      ctx.reply('У вас и так по четвертям', null, mainKeyboard);
    } else {
      ctx.reply('Всё, теперь у вас по четверям', null, mainKeyboard);
      DB.collection.updateOne({ _id: ctx.user._id }, {
        $set: {
          timeframe: 'quarter'
        }
      });
    }
  });
  bot.command(['Не могу войти'], (ctx) => {
    ctx.scene.enter('help');
  });
  bot.command('Выйти', (ctx) => {
    log.debug(`Exit ${ctx.message.peer_id}`);
    ctx.scene.enter('exit');
  });
};


