﻿import { getHalfStarts } from '../../modules/data/half';
import { getMarksImage } from '../../modules/marks';
import { permit } from '../permissions';
import { BotParams } from '../../types';
import log from '../../log';

export default ({ bot, DB, vk }: BotParams) => {
  const { firstHalf, secondHalf, endHalf } = getHalfStarts();

  bot.command('1 полугодие', permit('done'), (ctx) => {
    if (!ctx.done) return;
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, firstHalf, secondHalf, ctx.theme, '1 Полугодие').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за первое полугодие');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за первое полугодие');
    });

    log.debug(`1 полугодие ${ctx.message.peer_id}`);
  });

  bot.command('2 полугодие', permit('logged'), (ctx) => {
    ctx.typing();
    getMarksImage(ctx.user, ctx.message.peer_id, DB, vk, secondHalf, endHalf, ctx.theme, '2 Полугодие').then((photo) => {
      if (photo) {
        ctx.reply('', photo);
      } else {
        ctx.reply('У вас нет оценок за второе полугодие');
      }
    }).catch((err) => {
      ctx.reply('Произошла какая-то ошибка при получение оценок за второе полугодие');
    });
    log.debug(`2 полугодие ${ctx.message.peer_id}`);
  });
};
