﻿import assert from 'assert';
import mongodb from 'mongodb';
import fs from 'fs';
import * as Sentry from '@sentry/node';
import log from './log';

/*
  Basic microservice setup for lilDiary
*/


export default (): Promise<any> => {
  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
  }
  let mongourl: string;
  if (process.env.MONGOURL) {
    mongourl = process.env.MONGOURL;
  } else {
    const config = JSON.parse(fs.readFileSync('./config.json', { encoding: 'utf-8' }));
    mongourl = config.mongourl;
  }
  let commit = 'Нет данных';
  if (fs.existsSync('./commit.txt')) {
    commit = fs.readFileSync('./commit.txt', 'utf-8');
  }
  return new Promise((resolve) => {
    const { MongoClient } = mongodb;
    const mongodbClient = new MongoClient(mongourl, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    mongodbClient.connect((err) => {
      assert.strictEqual(null, err);
      const db = mongodbClient.db('lildiary');
      db.collection('config').findOne(
        {
          service: 'bot'
        },
        (err, conf) => {
          assert.strictEqual(null, err);
          assert.notStrictEqual(null, conf);

          const newConfig = { commit, service: 'bot', ...conf.config };

          db.collection('config').updateOne(
            {
              _id: conf._id
            },
            {
              $set: {
                lastStart: new Date(),
                ping: new Date()
              }
            }
          );
          setInterval(() => {
            db.collection('config').findOne({ service: 'bot' }).then((conf) => {
              for (const configKey in newConfig) {
                if (conf.config[configKey]) {
                  newConfig[configKey] = conf.config[configKey];
                }
              }
            }).catch((err: Error) => {
              throw err;
            });
          }, 10000);

          Sentry.init({
            dsn: newConfig.dsn,
            release: newConfig.service,
            environment: process.env.NODE_ENV
          });
          newConfig.work = true;
          resolve({ db, config: newConfig });
        }
      );
    });
  });
};
