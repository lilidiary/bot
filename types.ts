import { VkBot } from 'nodejs-vk-bot';
import { VK } from 'vk-io';
import DbClass from './modules/db';
import { Request } from 'express';
interface RequestMid {
  vkData: any;
  user: any;
}
type RequestData = Request & RequestMid;

interface ApiParams {
  config: any;
  DB: any;
  vk: VK;
}

interface BotParams {
  config: any;
  DB: any;
  vk: VK;
  bot: VkBot;
  regions?: any;
}
// interface user {
//   _id: ObjectID;
//   vkID: number;
//   added: Date;
//   fname: string;
//   sname: string;
//   passcode: string;
//   logged: boolean;
//   done: boolean;
//   url: string;
//   diaryData: any;
//   ip: string;
//   loggedTime: Date;
//   notification: {};
//   sessionid: string;
//   timeFrame: 'half' | 'quarter';
//   fun: number;
//   lastChecked: Date;
//   doneTime: Date;
//   marksCount: number;
//   marksDate: Date;
//   marksHash: number;
//   marksid: string;
//   fails: number
// }
export { BotParams, DbClass, VK, RequestData, ApiParams };
