import VkIO from 'vk-io';
import log from './log';
import DBClass from './modules/db';
import bot from './bot/index';
import setup from './setup';
import { startServer } from './api';

setup().then(({ db, config }) => {
  log.info('Starting vk bot');
  const DB = new DBClass(db);
  const vk = new VkIO({
    token: config.vk.bot_token
  });
  bot({ DB, config, vk });
  startServer({ DB, config, vk });
});
