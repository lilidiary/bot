import request from 'request';
import { Template } from '../../modules/svgTemplates';
import log from '../../log';
import { ApiParams, RequestData } from '../../types';
import { Response } from 'express';
const svgToImg = require('svg-to-img');

export function uploadStories({ DB, vk }: ApiParams) {
  return (req, res: Response) => {
    const { url } = req.body;
    const { user } = req;

    vk.api.users.get({ fields: ['photo_200'], user_ids: [req.user.vkID.toString()] }).then(([vkuser]) => {
      if (!user || !user.logged || !user.ratingPlaceClass) {
        res.json({ success: false, error: 'not logged' });
        return;
      }
      const tml = new Template('stories');
      if (user.classYear < 10) {
        tml.loadFile('stories.txt', 'base');
      } else {
        tml.loadFile('stories10.txt', 'base');
      }
      request.get(<string>vkuser.photo_200, { encoding: null }, (error, response, body) => {
        let image = '';
        if (!error && response.statusCode === 200) {
          image = body.toString('base64');
        }
        tml.render({ image, place: user.ratingPlaceClass + 1 });
        svgToImg.from(tml.svg).toJpeg({}).then((image: Buffer) => {
          vk.upload.buildPayload({
            maxFiles: 1,
            field: 'file',
            attachmentType: 'photo',
            values: [image]
          }).then((formData) => {
            vk.upload.upload(url, { formData, forceBuffer: false, timeout: null }).then((result) => {
              DB.collection.updateOne({ _id: user._id }, { $set: { story: true } });
            }).catch((err) => {
              log.error(err);
              res.json({ success: false });
            });
          }).catch((err) => {
            log.error(err);
          });
        }).catch((err) => {
          log.error(err);
          res.json({ success: false, error: 'unknown' });
        });
      });
    }).catch((err) => {
      log.error(err);
    });
  };
}
