import express from 'express';
import { uploadStories } from './stories';
import { ApiParams } from '../../types';
import { userMiddleware } from './user';

export function createAppRoutes(params: ApiParams) {
  const router = express.Router();
  router.use(userMiddleware(params));
  router.post('/stories', uploadStories(params));
  return router;
}
