import { Request, Response, NextFunction } from 'express';
import { verifyHash } from './verifyHash';
import { ApiParams } from '../../types';
import log from '../../log';

export function userMiddleware({ config, DB }: ApiParams) {
  return (req: Request, res: Response, next: NextFunction) => {
    const { vkData } = req.body;
    if (!vkData) {
      res.status(401).json({ success: false });
      return;
    }
    const isCorrectSignature = verifyHash(vkData, config.secretKey);
    if (!isCorrectSignature) {
      res.status(401).json({ success: false });
      return;
    }
    // @ts-ignore
    req.vkData = vkData;
    DB.findOne({ vkID: vkData.vk_user_id }).then((user) => {
      // @ts-ignore
      req.user = user;
      next();
    }).catch((err: Error) => {
      res.status(500).json({ success: false });
      log.error(err);
    });
  };
}
