import express from 'express';
import bodyparser from 'body-parser';
import cors from 'cors';
import { createAppRoutes } from './app/index';
import { ApiParams } from '../types';


export function startServer(params: ApiParams) {
  const { config } = params;

  const app = express();
  app.use(cors());
  app.use(bodyparser.json());

  app.get(['/readiness_check', '/liveness_check', '/'], (req, res) => {
    res.json({ success: true });
  });

  app.use('/app', createAppRoutes(params));

  app.use((req, res, next) => {
    res.status(404).json({ error: 404 });
    next();
  });
  app.listen(config.port);
}
