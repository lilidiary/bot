FROM registry.gitlab.com/lilidiary/bot:base
LABEL maintainer="Maksim"
ENV NODE_ENV=production

COPY . /app
WORKDIR /app
RUN npm install --production
RUN npm install typescript copyfiles -g 
RUN npm run tsc
RUN npm run prod
RUN npm remove typescript copyfiles -g 



# RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
#   && mkdir -p /home/pptruser/Downloads \
#   && chown -R pptruser:pptruser /home/pptruser \
#   && chown -R pptruser:pptruser /app

# USER pptruser

CMD [ "npm", "start" ]
